module.exports = {
  unit: {
    options: {
      reporter: 'spec',
      timeout: 3000,
      quiet: false, // Optionally suppress output to standard out (defaults to false)
      clearRequireCache: false // Optionally clear the require cache before running tests (defaults to false)
    },
    src: ['test/*Test.js']
  }
};