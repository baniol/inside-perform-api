'use strict';

var request = require('supertest');

var server;

var chai = require('chai');
var expect = chai.expect;

describe('AMQ Tests', function () {

  beforeEach(() => {
    delete require.cache[require.resolve('../bin/www')];
    server = require('../bin/www');
  });

  describe('...', () => {

    it('...', done => {
      request(server)
        .get('/api/getPeople')
        .expect('Content-Type', /application\/json/)
        .expect(res => {
          expect(res.body[0].id).to.equal(1);
        })
        .expect(200, done);
    });

  });

});