var mysql = require('mysql');

var config = require('../config');

var pool = mysql.createPool({
  host: config.mysql.host,
  user: config.mysql.user,
  password: config.mysql.password,
  database: config.mysql.database
});

exports.query = function (query) {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (err, connection) {
      if(err) {
        reject(err);
      }
      connection.query(query, (qError, rows) => {
        if(qError) {
          reject(qError);
        }
        else {
          resolve(rows);
        }
        connection.release();
      });
    });
  });
};

