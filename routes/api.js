var express = require('express');
var router = express.Router();

var db = require('../lib/mysql');

router.get('/getPeople', getPeople);
router.get('/getPerson/:id', getPerson);
router.get('/getDesks', getDesks);

function getPeople(req, res) {
  var query = `SELECT people.*, teams.name as teamName, teams.logo as teamLogo,
    projects.productName, projects.projectName, projects.logo as projectLogo
    FROM people
    LEFT JOIN teams ON teams.id = people.teamId
    LEFT JOIN projects ON projects.id = people.projectId`;

  returnQuery(query, res);
}

function getPerson(req, res) {
  var id = req.params.id;
  var query = `SELECT people.*, teams.name as teamName, teams.logo as teamLogo,
    projects.productName, projects.projectName, projects.logo as projectLogo
    FROM people
    LEFT JOIN teams ON teams.id = people.teamId
    LEFT JOIN projects ON projects.id = people.projectId
    WHERE people.id = ${id}`;

  returnQuery(query, res);
}

function getDesks(req, res) {
  var query = `SELECT *
  FROM desks`;

  returnQuery(query, res);
}

function returnQuery(query, res) {
  db.query(query)
    .then(queryResult => res.json(queryResult))
    .catch(err => res.json(err));
}


module.exports = router;
