module.exports = function (grunt) {

  var config = {
    pkg: grunt.file.readJSON('package.json'),
  };

  function loadConfig(path) {
    var glob = require('glob');
    var object = {};
    var key;

    glob.sync('*', {
      cwd: path
    }).forEach(function (option) {
      key = option.replace(/\.js$/, '');
      object[key] = require(path + option);
    });
    return object;
  }

  grunt.util._.extend(config, loadConfig('./grunt/'));

  grunt.initConfig(config);

  require('load-grunt-tasks')(grunt);
  //grunt.registerTask('default', ['test']);
  //grunt.registerTask('lint', ['eslint']);
  grunt.registerTask('test', ['mochaTest:unit']);
  //grunt.registerTask('test', ['githooks', 'lint', 'mochaTest:unit']);

};